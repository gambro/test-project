class CreateRates < ActiveRecord::Migration
  def change
    create_table :rates do |t|
      t.date   :date
      t.float  :rate
      t.string :status

      t.timestamps null: false
    end
  end
end
