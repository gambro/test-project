require 'spec_helper'

describe Parse::Ecb do

  let(:path) { "https://sdw-wsrest.ecb.europa.eu/service/data/EXR/D.USD.EUR.SP00.A" }
  subject { described_class.new }

  context '#initialize' do
    specify do
      expect(subject.url).to eq(path)
    end
  end

  context '#doc' do
    specify do
      expect(subject.send(:doc).class).to eq(Hash)
    end
  end

  context '#rates' do
    let(:parse_data) {
      parse_json = JSON.parse File.read(Rails.root.join 'spec/fixtures/' + 'test.json')
      parse_json["doc"]["GenericData"]["DataSet"]["Series"]["Obs"]
    }
    subject { parse_data.map{|n| Parse::Ecb::Rates.new(n["ObsDimension"]["value"], n)} }

    specify do
      expect(subject.class).to eq(Array)
      expect(subject.first.class).to eq(Parse::Ecb::Rates)
    end
  end
end
