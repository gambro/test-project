require 'rails_helper'

RSpec.describe Rate, type: :model do
  describe 'validations' do
    subject(:rate) { create :rate }

    specify do
      [:date, :rate, :status].each do |attribute|
        expect(subject).to validate_presence_of(attribute)
      end
      expect(subject).to validate_uniqueness_of(:date)
    end
  end

end
