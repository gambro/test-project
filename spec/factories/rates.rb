FactoryGirl.define do
  factory :rate do
    date Date.current
    rate 1.222
    status "MyString"
  end
end
