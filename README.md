# README #

### test-project ###

### install project ###

* git clone
* bundle install
* rake db:migrate
* rake test:prepare


### run project ###

* rails c
* download and save data to db: Parse::Ecb.new.store_rates
* form CSV file to folder ("test-project/tmp/downloads/#{Time.zone.today}.csv"): Rate.export_to_csv
* exchange rate: Rate.exchanger(amount, date); for example: 
  Rate.exchanger(100, '2017-06-09') => [111.76],
  Rate.exchanger(100, '2017-06-08', '2017-06-09') => [112.29, 111.76]

### run test ###

* rspec spec