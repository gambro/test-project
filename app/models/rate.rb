class Rate < ActiveRecord::Base
  validates :date, :rate, :status, presence: true
  validates :date, uniqueness: true

  def store!(remote_rate)
    update_attributes(
      date: remote_rate.date.to_date,
      rate: remote_rate.exchange_rate.to_f,
      status: remote_rate.status
    ) if new_record?
  end

  def self.exchanger(amount, *dates)
    return nil if amount == nil || amount == ""
    exchange_rate_by_date = []

    dates.each do |date|
      exchange_rate_by_date << (amount * Rate.find_by_date(date).rate).round(2)
    end

    exchange_rate_by_date
  end

  def self.export_to_csv
    filename = "#{Time.zone.today}.csv"
    File.open(Rails.application.config.downloads_folder + filename, 'w') do |f|
      f.write(Rate.to_csv)
    end
  end

  private

  def self.to_csv
    attributes = %w{date rate status}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.find_each do |r|
        csv << attributes.map{ |attr| r.send(attr) }
      end
    end
  end
end
