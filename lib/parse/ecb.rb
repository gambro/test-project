module Parse
  class Ecb
    require 'open-uri'

    attr_reader :url

    def initialize
      @url = "#{Settings.ecb.base_url}#{Settings.ecb.series_key}"
    end

    def rates
      elements.map{|n| Parse::Ecb::Rates.new(n["ObsDimension"]["value"], n)}
    end

    def store_rates
      rates.each do |rate|
        local_rate = ::Rate.find_or_initialize_by(date: rate.date.to_date)
        local_rate.store!(rate)
      end
    end

    private

    def doc
      @doc ||= ActiveSupport::XmlMini.parse(open(url))
    end

    def elements
      doc["GenericData"]["DataSet"]["Series"]["Obs"]
    end
  end
end
