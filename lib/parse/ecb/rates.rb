module Parse
  class Ecb
    class Rates
      attr_accessor :rate, :remote_date

      def initialize(remote_date, rate = {})
        @remote_date = remote_date
        @rate = rate
      end

      def date
        rate["ObsDimension"]["value"] || remote_date || ""
      end

      def exchange_rate
        rate["ObsValue"]["value"] || ""
      end

      def status
        rate["Attributes"]["Value"]["value"] || ""
      end
    end
  end
end
